<?php


class ValidateCode
{
    private $charset = "abcdefghjkmnprstuvwxyzABCDEFGHJKMNPRSTUVWXYZ23456789";          //提供的验证码字符集
    private $code;         //验证码
    private $codeLen = 4;       //生成的验证码长度
    private $width = 130;       //图片宽度
    private $height = 50;       //图片高度
    private $img;       //图像
    private $font;      //字体
    private $fontSize = 20;         //字体大小
    private $fontColor;             //字体颜色
//构造函数，设置验证码字体，生成一个真彩色图像img
    public function __construct(){
        $this->font = dirname(__FILE__).'/font/elephant.ttf';            //想要使用的TrueType字体的路径
        $this->img = imagecreatetruecolor($this->width, $this->height);         //创建一个新的真彩色图像赋值给成员变量
    }
//从字符集中随机抽取4个字符，生成验证码
    private function createCode(){
        $_len = strlen($this->charset) - 1;
        for ($i = 0; $i < $this->codeLen; $i++){
            $this->code .= $this->charset[mt_rand(0, $_len)];
        }
        return $this->code;
    }
//生成背景
    private function createBg(){
        $color = imagecolorallocate($this->img, mt_rand(157, 255), mt_rand(157, 255), mt_rand(157, 255));           //生成图像背景色
        imagefilledrectangle($this->img, 0, 0, $this->width, $this->height, $color);                  //生成矩形并用背景色填充
    }
//在图像上生成文字
    private function createFont(){
        $_x = $this->width/$this->codeLen;
        $_y = $this->height/2;
        for ($i = 0; $i < $this->codeLen; $i++){
            $this->fontColor = imagecolorallocate($this->img, mt_rand(0, 156), mt_rand(0, 156), mt_rand(0, 156));
            imagettftext($this->img, $this->fontSize, mt_rand(-30, 30), $_x * $i + mt_rand(3, 5), $_y + mt_rand(2, 4), $this->fontColor, $this->font, $this->code[$i]);
        }
    }
//在图像上生成线条和雪花
    private function createLine(){
        for ($i = 0; $i < 15; $i++){
            $color = imagecolorallocate($this->img, mt_rand(0, 156), mt_rand(0, 156), mt_rand(0, 156));
            imageline($this->img, mt_rand(0, $this->width), mt_rand(0, $this->height), mt_rand(0, $this->width), mt_rand(0, $this->height),$color);
        }
        for ($i = 0; $i < 150; $i++){
            $color = imagecolorallocate($this->img, mt_rand(200, 255), mt_rand(200, 255), mt_rand(200, 255));
            imagestring($this->img, mt_rand(1, 5), mt_rand(0, $this->width), mt_rand(0, $this->height), '#', $color);
        }
    }
//输出图像
    private function outPut(){
        header('Content-Type: image/png');
        imagepng($this->img);
        imagedestroy($this->img);
    }
//生成完整的验证码图像，供外部调用
    public function doImg(){
        $this->createBg();          //创建验证码背景
        $this->createCode();        //生成随机码
        $this->createLine();        //生成线条和雪花
        $this->createFont();        //生成文字
        $this->outPut();            //输出验证码图像
    }
//获取验证码
    public function getCode(){
        $this->createCode();
        return strtolower($this->code);
    }
}